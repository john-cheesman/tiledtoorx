﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Configuration;

namespace TiledToOrx
{
    public partial class ConverterForm : Form
    {

        FileSystemWatcher watcher = new FileSystemWatcher();

        StringBuilder errors = new StringBuilder();

        const string TAB = "\t";
        const string CR = "\n";
        const string AT = "@";

        readonly string version = "";

        public ConverterForm()
        {
            InitializeComponent();

            version = ConfigurationManager.AppSettings.Get("version"); 

            this.Text += version;
            watcher.Changed += Watcher_Changed;

        }

        private void Watcher_Changed(object sender, FileSystemEventArgs e)
        {
            if (e.ChangeType == WatcherChangeTypes.Changed)
            {
                this.Invoke((MethodInvoker)delegate {
                    lastLoadedTextbox.Text = "Last loaded " + DateTime.Now.ToLocalTime();
                });
                CheckAndEnableAndUpdatePreview();
            }
        }

        private string ConvertTMXtoOrx()
        {
            if (tmxTextBox.Text.Length == 0)
                return "";

            StringBuilder sb = new StringBuilder();

            try
            {
                FileStream file = File.Open(tmxTextBox.Text, FileMode.Open);
                XmlReader reader = XmlReader.Create(file);

                watcher.Path = tmxTextBox.Text.Replace(SourceToFileName(tmxTextBox.Text), "");
                watcher.Filter = SourceToFileName(tmxTextBox.Text);
                watcher.EnableRaisingEvents = true;

                Map map = SerialiseToMap(reader);

                file.Close();

                if (errors.Length > 0)
                {
                    MessageBox.Show(errors.ToString());
                    return "";
                }

                if (errors.Length == 0)
                {
                    CreateGraphicEntries(sb, map);
                }

            }
            catch (FileNotFoundException ex)
            {
                watcher.EnableRaisingEvents = false;
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                watcher.EnableRaisingEvents = false;
                MessageBox.Show("An Error occured loading and converting the TMX. Specific error: " + ex.Message);
            }



            return sb.ToString();
        }

        private Map SerialiseToMap(XmlReader reader)
        {
            XmlSerializer serialiser = new XmlSerializer(typeof(Map));
            Map map = (Map)serialiser.Deserialize(reader);

            //If any xml tile data, convert it. This is post process due to a bug in the deserializer library
            foreach (Layer layer in map.layers)
            {
                if (layer.data.tiles.Count > 0) //there are xml tiles
                {
                    for (int x = 0; x < layer.data.tiles.Count; x++)
                    {
                        layer.data.mapData.Add(layer.data.tiles[x].gid);
                    }
                }
            }
            

            return map;
        }

        private string GetTileSetNameByIndex(Map map, Int64 index)
        {
            foreach (TileSet tileSet in map.tileSets)
            {
                if (index >= tileSet.firstgid && index <= (tileSet.firstgid + tileSet.tileCount - 1) )
                {
                    return tileSet.name;
                }
            }
            return "NONE"; //return blank if no tile defined. ERROR RETURNS ?
        }

        private void CreateMap(StringBuilder sb, Map map, HashSet<Tile> tilesInUse)
        {
            const int limitPerLine = 32;

            foreach(Layer layer in map.layers)
            {
                int mapSpreadIndex = 1;

                sb.AppendLine(FormatConfigHeading(layer.name));

                string mapList = "";

                int lineLimitIndex = 0;

                for (int index = 0; index < layer.data.mapData.Count; index++)
                {
                    if (index > 0 && lineLimitIndex < limitPerLine)
                        mapList += "#";

                    if (lineLimitIndex == limitPerLine)
                    {
                        sb.AppendLine(FormatConfigProperty("MapPart" + mapSpreadIndex.ToString(), mapList));
                        mapSpreadIndex++;
                        lineLimitIndex = 0;
                        mapList = "";
                    }

                    lineLimitIndex++;
                    if (layer.data.mapData[index] == 0) //0 in the mapdata means no tile placed... essentially null
                    {
                        mapList += "NONE ";
                    } else
                    {
                        mapList += GetTileSetNameByIndex(map, layer.data.mapData[index]) + (layer.data.mapData[index] + PlusIndex()).ToString() + " ";
                    }
                }

                sb.AppendLine(FormatConfigProperty("MapPart" + mapSpreadIndex.ToString(), mapList));
                sb.AppendLine("");
            }

        }

        string FormatConfigHeading(string headingText)
        {
            return String.Format("[{0}]", headingText);
        }

        string FormatConfigProperty(string parameter, string value)
        {
            return String.Format("{0}{1}= {2}", parameter, TAB, value);
        }

        /// <summary>
        /// Remove paths from a path + file.
        /// </summary>
        /// <param name=""></param>
        /// <returns>Just the filename</returns>
        private string SourceToFileName(string pathAndFile)
        {
            string[] pathFragments = pathAndFile.Split('/');
            if (pathFragments.Length == 1)
            {
                pathFragments = pathAndFile.Split('\\');
            }
            string fileName = pathFragments[pathFragments.Length - 1];

            return fileName;
        }

        private int PlusIndex()
        {
            return oneBasedCheckBox.Checked ? 0 : -1;
        }

        /// <summary>
        ///  Only generate the Graphic Sections, and maps
        /// </summary>
        /// <param name="sb"></param>
        private void CreateGraphicEntries(StringBuilder sb, Map map)
        {
            HashSet<Tile> tilesInUse = new HashSet<Tile>();

            //Populate tilesInUse
            foreach (TileSet tileSet in map.tileSets)
            {
                for (int index=0; index < tileSet.tileCount; index++ )
                {
                    Tile tile = new Tile();
                    tile.index = index + tileSet.firstgid;
                    tile.X = (index % tileSet.columns) * tileSet.tileWidth;
                    tile.Y = (index / tileSet.columns) * tileSet.tileHeight; //row * tileSet.tileHeight;
                    tile.TileSetName = tileSet.name;

                    //got a tile but now lets see if it's used in one of the layers

                    for (int i = 0; i < map.layers.Count; i++)
                    {
                        Layer layer = map.layers[i];

                        if (layer.data.mapData.Contains(tile.index))
                        {
                            tilesInUse.Add(tile);
                        }
                    }

                }
            }


            //Print the graphic section and texture properties
            if (mapOnlyCheckBox.Checked == false)
            {
                foreach (TileSet tileSet in map.tileSets)
                {
                    sb.AppendLine(FormatConfigHeading(tileSet.name + "Graphic"));
                    sb.AppendLine(FormatConfigProperty("Texture", SourceToFileName(tileSet.image.source)));
                    sb.AppendLine(FormatConfigProperty("Pivot", "top left"));

                    string textureSizeValue = String.Format("({0}, {1}, 0)", map.tileWidth, map.tileHeight);
                    sb.AppendLine(FormatConfigProperty("TextureSize", textureSizeValue));

                    sb.AppendLine("");

                }

                //Output cut up graphic sections that the map list will use
                foreach (Tile tile in tilesInUse)
                {
                    if (tile == null)
                    {
                        errors.AppendFormat("Having difficulty creating a Orx Graphic Config Section at tileset position x:{0} y:{1}", tile.X, tile.Y);
                        return;
                    }

                    sb.AppendLine(FormatConfigHeading(tile.TileSetName + (tile.index + PlusIndex()).ToString() + "Graphic" + AT + tile.TileSetName + "Graphic")); 

                    string textureCornerValue = String.Format("({0}, {1}, 0)", tile.X, tile.Y);
                    sb.AppendLine(FormatConfigProperty("TextureOrigin", textureCornerValue));

                    sb.AppendLine("");
                }

                //create a default object
                sb.AppendLine(FormatConfigHeading("DefaultTileObject"));
                sb.AppendLine("");

                //Output object sections 
                foreach (Tile tile in tilesInUse)
                {
                    if (tile == null)
                    {
                        errors.AppendFormat("Having difficulty creating a Orx Object Config Section at tileset position x:{0} y:{1}", tile.X, tile.Y);
                        return;
                    }

                    sb.AppendLine(FormatConfigHeading(tile.TileSetName + (tile.index + PlusIndex()).ToString() + "Object" + AT + "DefaultTileObject"));

                    string graphic = tile.TileSetName + (tile.index + PlusIndex()).ToString() + "Graphic";
                    sb.AppendLine(FormatConfigProperty("Graphic", graphic));

                    sb.AppendLine("");
                }
            }

            //Create map outputs
            CreateMap(sb, map, tilesInUse);
        }

        private void tiledBrowseButton_Click(object sender, EventArgs e)
        {

            OpenFileDialog dialog = new OpenFileDialog();
            DialogResult result = dialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                string tmxFilePath = dialog.FileName;

                tmxTextBox.Text = tmxFilePath;
                tmxTextBox.Enabled = true;

                clipboardButton.Enabled = true;

            }
        }

        private void clipboardButton_Click(object sender, EventArgs e)
        {
            Clipboard.Clear();

            string output = "";

            if (previewBox.SelectionLength == 0)
            {
                output = previewBox.Text;
            }
            else
            {
                output = previewBox.SelectedText;
            }

            Clipboard.SetText(output);
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Tiled TMX to Orx Config v" + version + "\n2017 Wayne Johnson\nsausage@zeta.org.au");
        }

        private void helpToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            string message = @"
    
This converter accepts a Tiled file and will generate Orx Config Data. All map formats are accepted: CSV, XML, Base64, Base64/gzip and Base64/zlib.

Use the loader button to choose a TMX file.

Conversion will appear in the window. You change the formatting with the checkbox options.

Check the 'Map Only' option if you want to generate only the map data. Good for repeat conversions without needing the graphic config info.

Check the '1 Based Index' checkbox if you want your map sections to begin with 0 or 1.

The TMX file that you select will be watched. If you continue to work on your map within Tiled Map Editor, it will update in the converter. Therefore you can keep the converter open to receive the latest changes.

Send bugs to: sausage@zeta.org.au

            ";

            MessageBox.Show(message);
        }

        private void CheckAndEnableAndUpdatePreview()
        {
            if (tmxTextBox.Text.Length == 0)
            {
                checkboxPanel.Enabled = false;
                clipboardButton.Enabled = false;
            }
            else
            {
                checkboxPanel.Enabled = true;
                clipboardButton.Enabled = true;
            }

            UpdatePreview();

        }

        private void UpdatePreview()
        {
            string output = ConvertTMXtoOrx();
            if (output.Length == 0)
            {
                this.Invoke((MethodInvoker)delegate {
                    previewBox.Text = "Did not convert.";
                });
            }
            else
            {
                this.Invoke((MethodInvoker)delegate {
                    previewBox.Text = output;
                });
            }
        }

        private void tmxTextBox_TextChanged(object sender, EventArgs e)
        {
            CheckAndEnableAndUpdatePreview();
        }

        private void oneBasedCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            CheckAndEnableAndUpdatePreview();
        }

        private void mapOnlyCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            CheckAndEnableAndUpdatePreview();
        }

        private void previewBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.A)
            {
                previewBox.SelectAll();
            }
        }
    }
}