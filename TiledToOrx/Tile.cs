﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TiledToOrx
{
    public class Tile
    {
        public int index { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public string TileSetName { get; set; }
    }
}
